extern crate flatc_rust;
use std::path::{Path, PathBuf};
use std::fs::*;
use std::io::*;
type R = std::io::Result<()>;

fn main() {
  create_dir_all("target/flatbuffers").expect("create dir flatbuffers");
  build_genres().expect("build genres");
  build_langs().expect("build langs");
  flatc().expect("flatc");
}

fn build_genres() -> R {
  let reader = Cursor::new(include_str!("./manga-genres.txt"));
  println!("cargo:rerun-if-changed=manga-genres.txt");
  let lines: Vec<String> = reader.lines().filter_map(Result::ok).collect();
  serde_genres_write(&lines)?;
  Ok(())
}

fn serde_genres_write(lines: &Vec<String>) -> R {
  let mut file_out_rs = OpenOptions::new()
  .write(true)
  .truncate(true)
  .create(true)
  .open("target/flatbuffers/genres_serde.rs")?;
  writeln!(
    file_out_rs, "\n\
    use serde::Serialize;
    use serde_repr::*;\n\
    #[derive(Deserialize_repr, Serialize, PartialEq, PartialOrd, Copy, Clone)]\n\
    #[repr(u8)] pub enum MangaGenre {{\n  \
      Unknown = 0,\
    ")?;
  for (k, v) in lines.iter().enumerate() {
    writeln!(file_out_rs, "  {} = {},", v, k + 1)?;
  }
  writeln!(
    file_out_rs,
    "\
    }}\n\
    pub const MAX_MANGA_GENRE: u8 = {};\n\
    impl std::str::FromStr for MangaGenre {{\n  \
      type Err = std::num::ParseIntError;\n  \
      fn from_str(s: &str) -> Result<Self, Self::Err> {{\n    \
        match s.to_ascii_lowercase().as_str() {{\
    ",
    lines.len()
  )?;
  for v in lines.iter() {
    let tx = match v.as_str() {
      "FourKoma" => vec![
        "4-koma".to_string(),
        "four-koma".to_string(),
        "4koma".to_string(),
        "4 koma".to_string()
      ],
      "SciFi" => vec![
        "sci-fi".to_string(),
        "scifi".to_string(),
        "sci fi".to_string()
      ],
      _ => {
        let (k, w) = v.char_indices().fold(
          (String::with_capacity(v.len() + 3), String::with_capacity(v.len() + 3)),
          |(mut s_k, mut s_w), (j, c)| {
          if c.is_ascii_uppercase() && j != 0 { s_k.push('-'); s_w.push(' '); }
          s_k.push(c.to_ascii_lowercase());
          s_w.push(c.to_ascii_lowercase());
          (s_k, s_w)
        });
        if k != w { vec![k, w] } else { vec![k] }
      }
    }.iter().enumerate().fold(String::new(), |mut s, (i, v)| {
      if i != 0 { s.push_str(" | "); };
      s.push('"');
      s.push_str(v.as_str());
      s.push('"');
      s
    });
    writeln!(file_out_rs, "      {} => Ok(MangaGenre::{}),", tx, v)?;
  }
  file_out_rs.write_all(b"      \
          _ => Ok(s.parse::<u8>()?.into())\n    \
        }\n  \
      }\n\
    }\n\
    use std::fmt;\n\
    impl fmt::Display for MangaGenre {\n  \
      fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {\n    \
        match self {\n")?;
  for v in lines.iter() {
    let tx = match v.as_str() {
      "FourKoma" => String::from("4-Koma"),
      "PostApocalyptic" => String::from("Post-Apocalyptic"),
      "SciFi" => String::from("Sci-Fi"),
      _ => v.char_indices().fold(String::with_capacity(v.len() + 3), |mut s, (j, c)| {
        if c.is_ascii_uppercase() && j != 0 { s.push(' '); }
        s.push(c);
        s
      }),
    };
    writeln!(file_out_rs, "      MangaGenre::{} => write!(fmt, \"{}\"),", v, tx)?;
  }
  file_out_rs.write_all(b"      _ => write!(fmt, \"Unknown\"),\n    }\n  }\n}\n")?;
  file_out_rs.write_all(include_bytes!("builder/genre.rs"))?;
  file_out_rs.sync_all()
}

fn fbs_langs_write(lines: &Vec<MangaDexLanguage>) -> R {
  let mut file_out_fbs = OpenOptions::new()
  .write(true)
  .truncate(true)
  .create(true)
  .open("flatbuffers/langs.fbs")?;
  file_out_fbs.write_all(b"\
    namespace langs;\n\
    enum LangFlag : ubyte {\n  \
      other,\n\
  ")?;
  for lang in lines.iter() {
    writeln!(file_out_fbs, "  {}, // id {} - {}", lang.flag, lang.id, lang.name)?;
  }
  file_out_fbs.write_all(b"}\n")?;
  file_out_fbs.sync_all()
}

fn serde_langs_write(lines: &Vec<MangaDexLanguage>) -> R {
  let mut file_out_rs = OpenOptions::new()
  .write(true)
  .truncate(true)
  .create(true)
  .open("target/flatbuffers/langs_serde.rs")?;
  file_out_rs.write_all(b"\
    use serde::{Deserialize, Serialize};\n\
    #[allow(non_camel_case_types)]\n\
    #[derive(Deserialize, Serialize, PartialEq, PartialOrd, Copy, Clone)]\n\
    #[repr(u8)]\n\
    /// Language flags because Holo uses them... and also these deserialize easier.\n\
    pub enum LangFlag {\n  \
      /// No language for this\n  \
      other = 0,\n\
  ")?;
  for lang in lines.iter() {
    writeln!(file_out_rs, "  /// {}\n  {} = {},", lang.name, lang.flag, lang.id)?;
  }
  writeln!(
    file_out_rs,
    "  \
      /// All the languages\n  \
      all = 63\n\
    }}\n\
    pub const MAX_LANG_FLAG: u8 = {};\
    ",
    lines.len()
  )?;
  file_out_rs.write_all(b"\
    impl std::str::FromStr for LangFlag {\n  \
      type Err = !;\n  \
      fn from_str(s: &str) -> Result<Self, Self::Err> {\n    \
        match s.to_ascii_lowercase().as_str() {\n")?;
  for lang in lines.iter() {
    let MangaDexLanguage { code, flag, name, id } = lang;
    writeln!(
      file_out_rs,
      "      \"{}\" | \"{}\" => Ok(LangFlag::{}), // id = {}",
      code.to_ascii_lowercase(),
      name.to_ascii_lowercase(),
      flag,
      id
    )?;
  }
  file_out_rs.write_all(b"      \
          \"all\" => Ok(LangFlag::all), // 63\n      \
          _ => Ok(s.parse::<u8>().unwrap_or_default().into()), // 0\n    \
        }\n  \
      }\n\
    }\n\
    use std::fmt;\n\
    impl fmt::Display for LangFlag {\n  \
      fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {\n    \
        match self {\n")?;
  for lang in lines.iter() {
    writeln!(file_out_rs, "      LangFlag::{} => write!(fmt, \"{}\"),", lang.flag, lang.name)?;
  }
  file_out_rs.write_all(b"      LangFlag::all => write!(fmt, \"All\"),\n")?;
  file_out_rs.write_all(b"      _ => write!(fmt, \"Other\"),\n    }\n  }\n}\n")?;
  file_out_rs.write_all(include_bytes!("builder/lang.rs"))?;
  file_out_rs.sync_all()
}

struct MangaDexLanguage { name: String, code: String, flag: String, id: u8 }

fn build_langs() -> R {
  let reader = Cursor::new(include_str!("./manga-langs.txt"));
  println!("cargo:rerun-if-changed=manga-langs.txt");
  let lines = reader
  .lines()
  .filter_map(Result::ok)
  .filter_map(|s| {
    let k = s.trim();
    if k.is_empty() { None }
    else { Some(k.to_owned()) }
  })
  .collect::<Vec<String>>();
  let pieces: Vec<MangaDexLanguage> = lines
  .iter()
  .map(|v| {
    let ss: Vec<&str> = v.split(':').take(4).collect();
    let id: u8 = ss[0].parse().unwrap();
    let flag = ss[1].to_owned();
    let code = ss[2].to_owned();
    let name = ss[3].to_owned();
    MangaDexLanguage { id, flag, code, name }
  })
  .collect();
  fbs_langs_write(&pieces)?;
  serde_langs_write(&pieces)?;
  Ok(())
}

fn flatc() -> R {
  let files: Vec<PathBuf> = read_dir("./flatbuffers/")?
  .filter_map(Result::ok)
  .map(|v| v.path())
  .inspect(|v| {
    println!("cargo:rerun-if-changed={}", v.to_string_lossy());
  })
  .collect();
  flatc_rust::run(flatc_rust::Args {
    inputs: files.iter().map(|v| v.as_path()).collect::<Vec<&Path>>().as_slice(),
    out_dir: Path::new("target/flatbuffers/"),
    ..Default::default()
  })?;
  let mut file = OpenOptions::new()
    .append(true)
    .open("target/flatbuffers/manga_generated.rs")?;
  // append include modules and stuff since flatc doesn't do that
  file.write_all(include_bytes!("builder/mang.rs"))?;
  file.sync_all()
}
