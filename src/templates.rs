
extern crate maud;
use maud::*;

use crate::manga_decode::*;

use serde::de;
extern crate actix_web;
use actix_web::{Error, HttpResponse};
use actix_web::web::{Path, Query, Json};

//extern crate serde_derive;
//use serde_derive::Deserialize;
//#[derive(Deserialize)]
//struct Named { name: String }

fn slep() -> &'static str {
  // std::thread::sleep(std::time::Duration::SECOND);
  ""
}


fn hello(name: Option<&String>) -> Markup {
  html! {
    (DOCTYPE)
    html lang = "en" {
      head { meta charset = "utf-8"; }
      body {
        @match name {
            Some(v) => "Hello, " (v) "!",
            None => "Hello!",
        }
        br;
        (name.map_or("Hello!".into(), |v| {format!("Hello, {}!", v)}))
        br;
        (slep())
        div.hi {}
      }
    }
  }
}

pub fn named(req: Path<String>) -> Markup {
  let name = req.into_inner();
  let s = if name.trim().is_empty() { None } else { Some(&name) };
  hello(s)
}

pub fn index(_req: Path<()>) -> Markup {
  let s = None;
  hello(s)
}
type QueryList = Query<Vec<(String, String)>>;

impl From<QueryList> for MangaGenreSet {
  fn from(v: QueryList) -> Self {
    let mut slf = Self::default();
    for (key, val) in v.into_inner() {
      match key.as_str() {
        "genre" => if let Ok(k) = val.parse() { slf += k; },
        _ => {},
      }
    }
    slf
  }
}

pub fn genres_test(p: QueryList) -> String {
  print!("{:?}", p);
  let g = MangaGenreSet::from(p);
  println!(" :: {:?}", g);
  format!("{:?}", g)
}

impl From<QueryList> for LangFlagSet {
  fn from(v: QueryList) -> Self {
    let mut slf = Self::default();
    for (key, val) in v.into_inner() {
      match key.as_str() {
        "lang" => if let Ok(k) = val.parse() { slf += k; },
        _ => {},
      }
    }
    slf
  }
}

pub fn langs_test(p: QueryList) -> String {
  print!("{:?}", p);
  let g = LangFlagSet::from(p);
  println!(" :: {:?}", g);
  format!("{:?}", g)
}
use reqwest::r#async as rq;
use futures::{Future, future::*};
fn rq_json<'a, T>(
  url: &'a String,
  client: &'a rq::Client
) -> Result<T, ()> where T: de::DeserializeOwned {
  println!("requesting: {:?}", url);
  let mut res = client.get(url)
  .send()
  .wait()
  .map_err(|_| ())?;
  println!("response: {:?}", res);
  res
  .json()
  .wait()
  .map_err(|_| ())
}
pub fn manga_test(
  p: Path<u32>,
  client: &rq::Client
) -> impl Future<Item=Json<MangaDexManga>, Error=Error> {
  let g = p.into_inner();
  if g != 0 {
    println!("manga id: {}", g);
    let url = format!("https://mangadex.org/api/manga/{}", g);
    if let Ok(j) = rq_json(&url, &client) {
      return ok(Json(j));
    }
  }
  ok(Json(MangaDexManga::None))
}
