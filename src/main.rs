#![feature(proc_macro_hygiene, never_type)]

extern crate listenfd;
extern crate actix_web;
use listenfd::ListenFd;
use actix_web::{HttpServer, web, App};

mod templates;
use templates::*;
mod manga_decode;
use reqwest::{r#async as rq, header};
fn main() -> std::io::Result<()> {
  let mut lfd = ListenFd::from_env();
  let mut server = HttpServer::new(|| {
    let headers = {
      let mut h = header::HeaderMap::new();
      h.insert(header::USER_AGENT, "z3manga/0.1.0".parse().unwrap());
      h
    };
    let client = rq::Client::builder()
      .h2_prior_knowledge()
      .use_rustls_tls()
      .gzip(true)
      .cookie_store(true)
      .default_headers(headers)
      .build()
      .unwrap();
    App::new()
      .service(web::resource("/").route(web::get().to(index)))
      .service(web::resource("/{name}").route(web::get().to(named)))
      .service(
        web::scope("/testing")
        .service(web::resource("/genres").route(web::get().to(genres_test)))
        .service(web::resource("/langs").route(web::get().to(langs_test)))
        .service(web::resource("/manga/{manga}").route(web::get().to_async(move |p| {
          manga_test(p, &client)
        })))
      )
  });
  server = if let Some(l) = lfd.take_tcp_listener(0).unwrap() {
    server.listen(l)?
  } else {
    server.bind("127.0.0.1:6000")?
  };
  server.run()
}
