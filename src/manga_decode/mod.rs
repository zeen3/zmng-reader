extern crate serde;
use serde::{Deserialize, Serialize};
extern crate serde_repr;
use serde_repr::*;
use flatbuffers;
use std::collections::BTreeMap;
use std::num::NonZeroU32;
#[path = "../../target/flatbuffers/manga_generated.rs"]
mod manga;
pub use manga::{
  langs_serde::*,
  genres_serde::*,
  z3manga,
  enums,
  langs,
  MangaRelation,
};
pub trait ToFlatResp {
  fn to_resp(&self) -> Vec<u8>;
}


#[derive(Deserialize_repr, Serialize, Debug, Copy, Clone)] #[repr(u8)] pub enum MangaStatus {
  Unknown = 0,
  Ongoing = 1,
  Completed = 2,
  Cancelled = 3,
  OnHiatus = 4,
}
impl Default for MangaStatus { fn default() -> Self { MangaStatus::Unknown } }
impl From<enums::TitleStatus> for MangaStatus {
  fn from(v: enums::TitleStatus) -> Self {
    unsafe { std::mem::transmute(v) }
  }
}
impl From<MangaStatus> for enums::TitleStatus {
  fn from(v: MangaStatus) -> Self {
    unsafe { std::mem::transmute(v) }
  }
}

/// External links to tracking sites or places you can purchase it.
#[derive(Deserialize, Serialize, Default, Clone, Debug)] pub struct ExternalMangaLinks {
  /// MangaUpdates ID
  pub mu: Option<NonZeroU32>,
  /// MyAnimeList ID
  pub mal: Option<NonZeroU32>,
  /// NovelUpdates slug
  pub nu: Option<String>,
  /// Raw URL
  pub raw: Option<String>,
  /// Official Eng URL
  pub engtl: Option<String>,
  /// CDJapan URL
  pub cdj: Option<String>,
  /// Amazon.co.jp URL
  pub amz: Option<String>,
  /// eBookJapan URL
  pub ebj: Option<String>,
  /// Bookwalker ID
  pub bw: Option<String>,
}

/// Information pulled from manga.
#[derive(Deserialize, Serialize, Default, Clone, Debug)] pub struct MangaInfo {
  pub title: String,
  pub description: String,
  pub author: String,
  pub status: MangaStatus,
  pub genres: Vec<MangaGenre>,
  /// Last chapter published
  pub end_chapter: String,
  /// i.e. hentai, porn etc
  pub over_18: bool,
  /// uses long strip by default
  pub long_strip: bool,
  #[serde(rename = "lang_flag")]
  pub lang: LangFlag,
  pub links: ExternalMangaLinks,
  //pub relations: Vec<MangaRelation>,
}

//impl From<z3manga::Bytes> for MangaGenreSet {
//  fn from(b: z3manga::Bytes) -> Self {
//    MangaGenreSet::new(b.into())
//  }
//}

impl MangaInfo {
  pub fn manga_genres(&self) -> MangaGenreSet {
    let mut genres = MangaGenreSet::new(0);
    for v in self.genres.iter().cloned() { genres += v; }
    genres
  }
}
impl ToFlatResp for MangaInfo {
  fn to_resp(&self) -> Vec<u8> {
    let mut fbb = flatbuffers::FlatBufferBuilder::new_with_capacity(0x400);
    let name = fbb.create_string(&self.title);
    let desc = fbb.create_string(&self.description);
    let lang = self.lang.into();
    let genres = {
      let [
        a, b, c, d, e, f, g, h,
        i, j, k, l, m, n, o, p
      ] = self.manga_genres().to_le_u8_mask();
      z3manga::Bytes::new(
        a, b, c, d, e, f, g, h,
        i, j, k, l, m, n, o, p
      )
    };
    //let relations = if self.relations.len() > 0 {
    //  let rel = self.relations
    //  .iter()
    //  .filter_map(|v| {
    //    if !v.ok() { None } else {
    //      let name = fbb.create_string(&v.name);
    //      let mut r = z3manga::RelationBuilder::new(&mut fbb);
    //      r.add_related_by(v.by());
    //      r.add_is_hentai(v.is_hentai);
    //      r.add_rel_id(v.rel);
    //      r.add_title(name);
    //      Some(r.finish())
    //    }
    //  })
    //  .collect::<Vec<_>>();
    //  Some(fbb.create_vector(&rel))
    //} else { None };
    let end = if self.end_chapter != "0".to_string() {
      Some(fbb.create_string(&self.end_chapter))
    } else { None };
    let title = {
      let mut t = z3manga::TitleBuilder::new(&mut fbb);
      t.add_name(name);
      t.add_desc(desc);
      t.add_genres(&genres);
      t.add_over_18(self.over_18);
      t.add_long_strip(self.long_strip);
      t.add_original_lang(lang);
      t.add_status(self.status.into());
      if let Some(x) = end { t.add_end_chapter(x); };
      //if let Some(x) = relations { t.add_relations(x); };
      t.finish()
    };
    let resp = z3manga::Resp::create(&mut fbb, &z3manga::RespArgs {
      res: Some(title.as_union_value()),
      res_type: z3manga::UResp::Title
    });
    z3manga::finish_resp_buffer(&mut fbb, resp);
    let data = fbb.finished_data();
    data.to_vec()
  }
}

#[derive(Deserialize, Serialize)] pub struct MangaChapter {
  pub title: String,
  #[serde(rename = "lang_code")]
  pub lang: LangFlag,
  pub volume: String,
  pub chapter: String,
  pub group_id: NonZeroU32,
  pub group_name: String,
  pub group_id_2: Option<NonZeroU32>,
  pub group_name_2: Option<String>,
  pub group_id_3: Option<NonZeroU32>,
  pub group_name_3: Option<String>,
  pub timestamp: u64,
}
#[derive(Deserialize, Serialize)] #[serde(tag = "status")] pub enum MangaDexManga {
  #[serde(rename = "Manga ID does not exist.")]
  None,
  #[serde(rename = "OK")]
  Manga {
    manga: MangaInfo,
    #[serde(rename = "chapter")]
    chapters: BTreeMap<u32, MangaChapter>,
  },
}
