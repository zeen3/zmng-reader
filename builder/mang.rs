
// automatically added by build script
mod enums_generated;
pub use enums_generated::enums;
pub mod genres_serde;
mod langs_generated;
pub use langs_generated::langs;
pub mod langs_serde;
impl From<langs_serde::LangFlag> for langs::LangFlag {
  fn from(v: langs_serde::LangFlag) -> Self { unsafe { std::mem::transmute(v) } }
}
impl From<z3manga::Bytes> for [u8; 16] {
  fn from(b: z3manga::Bytes) -> Self {
    [
      b.mask_0(), b.mask_1(), b.mask_2(), b.mask_3(),
      b.mask_4(), b.mask_5(), b.mask_6(), b.mask_7(),
      b.mask_8(), b.mask_9(), b.mask_a(), b.mask_b(),
      b.mask_c(), b.mask_d(), b.mask_e(), b.mask_f()
    ]
  }
}
impl From<z3manga::Bytes> for u128 {
  fn from(b: z3manga::Bytes) -> Self {
    u128::from_le_bytes(b.into())
  }
}

use serde::Deserialize;
#[derive(Debug, PartialEq, PartialOrd, Deserialize, Clone)]
pub struct MangaRelation {
  pub rel: u32,
  pub related_by: u8,
  pub is_hentai: bool,
  pub name: String,
}
impl MangaRelation {
  pub fn new(id: u32, title: String, over18: bool, by: enums::RelatedBy) -> Self {
    MangaRelation {
      rel: id,
      related_by: by as u8,
      is_hentai: over18,
      name: title
    }
  }
  pub fn by(&self) -> enums::RelatedBy {
    unsafe { std::mem::transmute(self.related_by) }
  }
  pub fn ok(&self) -> bool {
    self.rel != 0 && self.related_by != 0 && self.name.len() != 0
  }
}

//impl From<&'_ z3manga::Relation> for MangaRelation {
//  fn from(v: &'_ z3manga::Relation) -> MangaRelation {
//    MangaRelation::new(
//      v.rel_id(),
//      v.title().to_string(),
//      v.is_hentai(),
//      v.related_by()
//    )
//  }
//}
//
