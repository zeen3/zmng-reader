// Inserted by the build.rs script.

impl Default for MangaGenre { fn default() -> Self { MangaGenre::Unknown } }
impl From<u8> for MangaGenre {
  fn from(v: u8) -> Self {
    if v > MAX_MANGA_GENRE { MangaGenre::Unknown }
    else { unsafe { std::mem::transmute(v) } }
  }
}
#[derive(Default)]
#[repr(transparent)]
pub struct MangaGenreSet(Option<std::num::NonZeroU128>);
impl MangaGenreSet {
  pub fn new(v: u128) -> Self {
    if v == 0 { Self(None) }
    else { unsafe { Self(Some(std::num::NonZeroU128::new_unchecked(v))) } }
  }
  pub fn is_none(&self) -> bool { self.0.is_none() }
  pub fn get(&self) -> u128 { if let Some(n) = self.0 { n.get() } else { 0 } }
  pub fn has(&self, genre: MangaGenre) -> bool {
    let g = genre as u8;
    (self.get() & (1 << g)) != 0
  }
  pub fn to_le_u8_mask(&self) -> [u8; 16] {
    self.get().to_le_bytes()
  }
}
use std::ops::*;
impl fmt::Debug for MangaGenre {
  fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
    //write!(fmt, "{:?}: {}", self.to_string(), *self as u8)
    write!(fmt, "{}", self.to_string())
  }
}
impl fmt::Debug for MangaGenreSet {
  fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "MangaGenreSet ")?;
    fmt.debug_set().entries((1..=MAX_MANGA_GENRE).filter_map(|v| {
      if self.has(v.into()) { Some(MangaGenre::from(v)) }
      else { None }
    })).finish()
  }
}
impl AddAssign<MangaGenre> for MangaGenreSet {
  fn add_assign(&mut self, rhs: MangaGenre) {
    let n = rhs as u8;
    *self = Self::new(self.get() | (1 << n));
  }
}
impl BitAnd<MangaGenreSet> for MangaGenreSet {
  type Output = u128;
  fn bitand(self, rhs: Self) -> u128 { self.get() & rhs.get() }
}
impl SubAssign<MangaGenre> for MangaGenreSet {
  fn sub_assign(&mut self, rhs: MangaGenre) {
    if !self.is_none() {
      let n = rhs as u8;
      *self = Self::new(self.get() & !(1 << n));
    }
  }
}
