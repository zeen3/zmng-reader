// Inserted by the build.rs script.

impl Default for LangFlag { fn default() -> Self { LangFlag::other } }
impl From<LangFlag> for u8 {fn from(v: LangFlag) -> u8 { v as u8 }}

impl From<u8> for LangFlag {
  fn from(v: u8) -> Self {
    if v > MAX_LANG_FLAG && v != 63 { LangFlag::other }
    else { unsafe { std::mem::transmute(v) } }
  }
}

use std::ops::*;
impl fmt::Debug for LangFlag {
  fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
    //write!(fmt, "{:?}: {}", self.to_string(), *self as u8)
    write!(fmt, "{}", self.to_string())
  }
}
#[derive(Default)]
#[repr(transparent)]
pub struct LangFlagSet(Option<std::num::NonZeroU64>);
impl LangFlagSet {
  pub fn new(v: u64) -> Self {
    if v == 0 { Self(None) }
    else { unsafe { Self(Some(std::num::NonZeroU64::new_unchecked(v))) } }
  }
  pub fn is_none(&self) -> bool { self.0.is_none() }
  pub fn get(&self) -> u64 { if let Some(n) = self.0 { n.get() } else { 0 } }
  pub fn has(&self, flag: LangFlag) -> bool {
    let g = flag as u8;
    (self.get() & (1 << g)) != 0
  }
}
impl fmt::Debug for LangFlagSet {
  fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "LangFlagSet ")?;
    if self.get() & (1 << 63) != 0 {
      fmt.debug_set().entries([LangFlag::all].iter()).finish()
    } else {
      fmt.debug_set().entries((0..=MAX_LANG_FLAG).filter_map(|v| {
        if self.has(v.into()) { Some(LangFlag::from(v)) }
        else { None }
      })).finish()
    }
  }
}
impl AddAssign<LangFlag> for LangFlagSet {
  fn add_assign(&mut self, rhs: LangFlag) {
    let n = rhs as u8;
    *self = Self::new(self.get() | (1 << n));
  }
}
impl BitAnd<LangFlagSet> for LangFlagSet {
  type Output = u64;
  fn bitand(self, rhs: Self) -> u64 { self.get() & rhs.get() }
}
impl SubAssign<LangFlag> for LangFlagSet {
  fn sub_assign(&mut self, rhs: LangFlag) {
    if !self.is_none() {
      let n = rhs as u8;
      *self = Self::new(self.get() & !(1 << n));
    }
  }
}
